import crypto from '@/plugins/cripto'

const state = () => ({
  user: null,
  menuList: [],
  validRegionList: [],
  allPremiumRateList: [],
})

const getters = {
  getUser: (state) => {
    return state.user
  },
  getMenuList: (state) => {
    return state.menuList
  },
  getValidRegionList: (state) => {
    return state.validRegionList
  },
  getAllPremiumRateList: (state) => {
    return state.allPremiumRateList
  },
}

const mutations = {
  commitUser: (state, user) => {
    state.user = user
  },
  commitMenuList: (state, list) => {
    state.menuList = list
  },
  commitValidRegionList: (state, list) => {
    state.validRegionList = list
  },
  commitAllPremiumRateList: (state, list) => {
    state.allPremiumRateList = list
  },
}

const actions = {
  // nuxtServerInit is called by Nuxt.js before server-rendering every page
  nuxtServerInit({ commit, dispatch }, { req }) {
    if (this.$cookies.get('veltra-token') && this.$cookies.get('veltra-user')) {
      commit('commitUser', {
        auth_token: this.$cookies.get('veltra-token'),
        user_name: this.$cookies.get('veltra-user'),
      })
    } else {
      commit('commitUser', null)
      dispatch('dispatchCheckToken')
      this.$cookies.remove('veltra-token')
    }
    dispatch('dispatchMenuList')
    dispatch('dispatchAllPremiumRateList')
    dispatch('dispatchDefaultToken')
  },
  async dispatchMenuList({ commit }, params) {
    console.log('dispatchMenuList')
    try {
      const menuList = await this.$axios.$get(`/api/v1/assets/menu.json`)
      commit('commitMenuList', menuList)
      return menuList
    } catch (e) {
      console.log(e)
      // error({ statusCode: e.response.status })
    }
  },
  async dispatchAllPremiumRateList({ commit }, params) {
    try {
      const allPremiumRateList = await this.$axios.$get(`/api/v1/cms/config/area/premium`, {
        params,
      })
      commit('commitAllPremiumRateList', allPremiumRateList)
      return allPremiumRateList
    } catch (e) {
      // throw new Error('Internal Server Error')
    }
  },
  async dispatchUser({ commit }, params) {
    try {
      const user = await this.$axios.$get(`/api/v1/members`)
      commit('commitUser', user)
      return user
    } catch (e) {
      // throw new Error('Internal Server Error')
    }
  },
  async dispatchFindUser({ commit }, params) {
    try {
      return await this.$axios.$get(`/api/v1/members/find`, {
        params,
      })
    } catch (e) {
      // throw new Error('Internal Server Error')
    }
  },
  async dispatchSendSms({ commit }, params) {
    try {
      return await this.$axios.$post(`/api/v1/members/auth/mobile`, {
        params,
      })
    } catch (e) {
      // throw new Error('Internal Server Error')
    }
  },
  async dispatchVerifyMobile({ commit }, params) {
    try {
      return await this.$axios.$put(`/api/v1/members/auth/mobile`, {
        params,
      })
    } catch (e) {
      // throw new Error('Internal Server Error')
    }
  },
  async dispatchUpdateContact({ commit }, params) {
    try {
      return await this.$axios.$put(`/api/v1/members/contact`, {
        params,
      })
    } catch (e) {
      // throw new Error('Internal Server Error')
    }
  },
  async dispatchUpdatePassword({ commit }, params) {
    try {
      return await this.$axios.$put(`/api/v1/members/${params.id}`, {
        params,
      })
    } catch (e) {
      // throw new Error('Internal Server Error')
    }
  },
  async dispatchSendResetMail({ commit }, params) {
    try {
      return await this.$axios.$post(`/api/v1/members/reset/password`, {
        params,
      })
    } catch (e) {
      // throw new Error('Internal Server Error')
    }
  },
  async dispatchVerifyResetHash({ commit }, { hash }) {
    try {
      return await this.$axios.$get(`/api/v1/members/hash/validation`, {
        hash,
      })
    } catch (e) {
      // throw new Error('Internal Server Error')
    }
  },
  async dispatchResetPassword({ commit }, params) {
    try {
      return await this.$axios.$put(`/api/v1/members/hash/password`, {
        params,
      })
    } catch (e) {
      // throw new Error('Internal Server Error')
    }
  },
  async dispatchWithDraw({ commit, dispatch }, params) {
    try {
      await this.$axios.$post(`/api/v1/members/withdraw`)
      await dispatch('dispatchLogout')
    } catch (e) {
      // throw new Error('Internal Server Error')
    }
  },
  async dispatchDefaultToken({ commit, dispatch }, params) {
    try {
      const tokenConfig = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': `Basic ${btoa('veltra:veltra-secret')}`,
        },
      }
      const formData = new FormData()
      formData.append('grant_type', 'client_credentials')
      formData.append('username', 'veltra')
      formData.append('password', 'veltra-secret')
      formData.append('scope', 'read')
      const { data } = await this.$axios.post('/oauth/token', formData, tokenConfig)
      console.log('tokenResult', data)

      const checkData = new FormData()
      checkData.append('token', data.access_token)
      this.$cookies.set('veltra-token', data.access_token)
      await this.$axios.post('/oauth/check_token', checkData, tokenConfig)

      return data
    } catch (error) {
      // throw error
    }
  },
  async dispatchSocialLogin({ commit, dispatch }, params) {
    try {
      const tokenResult = await dispatch('dispatchDefaultToken')
      const findUserResult = await dispatch('dispatchFindUser', {
        email: params.email,
      })
      if ((findUserResult.memberStatus === 'MEMBER' && findUserResult.sns && findUserResult.sns.indexOf('NAVER') >= 0)) {
        this.$cookies.set('veltra-token', tokenResult.access_token)
        this.$cookies.set('veltra-user', params.email)
        commit('commitUser', {
          user_name: params.email,
          access_token: tokenResult.access_token,
        })
        return true
      } else {
        return false
      }
    } catch (error) {
      console.log(error)
    }
  },
  async dispatchSignUp({ commit, dispatch }, params) {
    try {
      await this.$axios.$post(`/api/v1/members`, {
        ...params,
        access_token: this.$cookies.get('veltra-token'),
      })
      if (params.password) {
        return await dispatch('dispatchLogin', params)
      }
      commit('commitUser', {
        user_name: params.email,
        access_token: this.$cookies.get('veltra-token'),
      })
      return true
    } catch (e) {
      console.log(e.response)
      if (e.response && e.response.data && e.response.data.message === 'Exist User Id') {
        console.log('이미 존재하는 아이디입니다.')
      }
      // throw new Error('Internal Server Error')
    }
  },
  async dispatchLogin({ commit, dispatch }, params) {
    try {
      const findUserResult = await dispatch('dispatchFindUser', {
        email: params.email.trim(),
      })
      console.log('dispatchLogin findUserResult', findUserResult)
      if (!findUserResult.userId) {
        dispatch('common/dispatchPopup', {
          type: 'confirm',
          title: '로그인',
          message: '회원 정보가 존재하지 않습니다.<br>회원가입 페이지로 이동하시겠습니까?',
          callback: () => {
            this.$router.push(`/auth/signup?redirectUrl=${this.$cookies.get('veltra-redirect') || '/'}`)
            dispatch('common/dispatchClosePopup')
          },
        })
        return false
      }
      const authConfig = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': `Basic ${btoa('veltra:veltra-secret')}`,
        },
      }
      const formData = new FormData()
      formData.append('grant_type', 'password')
      formData.append('username', params.email && params.email.trim())
      formData.append('password', params.password)
      formData.append('scope', 'read')

      const { data } = await this.$axios.post('/oauth/token', formData, authConfig)
      console.log('tokenResult', data)
      commit('commitUser', data)

      await dispatch('dispatchCheckToken', data)
      await dispatch('dispatchLoginSuccess')
      return true
    } catch (error) {
      // return Promise.reject(error)
    }
  },
  async dispatchCheckToken({ commit }, user) {
    try {
      const authConfig = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': `Basic ${btoa('veltra:veltra-secret')}`,
        },
      }
      const formData = new FormData()
      formData.append('token', user.access_token)
      const { data } = await this.$axios.post('/oauth/check_token', formData, authConfig)
      console.log('check Token Result', data)
      this.$cookies.set('veltra-token', user.access_token)
      this.$cookies.set('veltra-user', data.user_name)
      this.$cookies.set('veltra-refresh', user.refresh_token)
      commit('commitUser', {
        ...user,
        ...data,
      })
    } catch (error) {
      if (error.response && error.response.status === 401) {
        // throw new Error('Bad credentials')
      }
      // throw error
    }
  },
  async dispatchLoginSuccess({ commit }, params) {
    try {
      const { name } = await this.$axios.$put(`/api/v1/members/login/success`, null, {
        headers: {
          'Content-Type': 'application/json',
        },
      })
      this.$cookies.set('veltra-n', crypto.encodeValue(name))
    } catch (error) {
      if (error.response && error.response.status === 401) {
        // throw new Error('Bad credentials')
      }
      // throw error
    }
  },
  dispatchLogout({ commit }) {
    console.log('this.$router.currentRoute.path', this.$router.currentRoute.path)
    commit('commitUser', null)
    this.$cookies.removeAll()
    if (process.env.NEED_AUTH_PAGE.indexOf(this.$router.currentRoute.path) >= 0) {
      this.$router.push('/')
    }
  },
}

export default {
  state,
  getters,
  mutations,
  actions,
}
