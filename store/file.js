const state = () => ({
})

// getters 는 서버사이드 렌더링 방식에서 먹히지 않음
const getters = {
}

const mutations = {
}

const actions = {
  async dispatchFile({ commit }, params) {
    const config = {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    }
    const formData = new FormData()
    formData.append('file', params)
    try {
      return await this.$axios.$post(`/api/v1/common/files`, formData, config)
    } catch (e) {
      // throw new Error('Internal Server Error')
    }
  },
}

export default {
  state,
  getters,
  mutations,
  actions,
}
