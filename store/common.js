const state = () => ({
  premiumRateList: [],
  slideList: [],
  isLoading: false,
  pageName: '',
  redirectUrl: '',
  popup: {
    type: 'alert',
    show: false,
    disableClickOutside: false,
    title: '',
    message: '',
    callback: null,
  },
})

// getters 는 서버사이드 렌더링 방식에서 먹히지 않음
const getters = {
  getPremiumRateList: (state) => {
    return state.premiumRateList
  },
  getSlideList: (state) => {
    return state.slideList
  },
  getIsLoading: (state) => {
    return state.isLoading
  },
  getPageName: (state) => {
    return state.pageName
  },
  getRedirectUrl: (state) => {
    return state.redirectUrl
  },
  getPopup: (state) => {
    return state.popup
  },
}

const mutations = {
  commitPremiumRateList: (state, list) => {
    state.premiumRateList = list
  },
  commitSlideList: (state, list) => {
    state.slideList = list
  },
  commitIsLoading: (state, boolean) => {
    state.isLoading = boolean
  },
  commitPageName: (state, string) => {
    state.pageName = string
  },
  commitRedirectUrl: (state, string) => {
    state.redirectUrl = string
  },
  commitPopup: (state, object) => {
    state.popup = object
  },
}

const actions = {
  async dispatchPremiumRateList({ commit }, params) {
    try {
      const premiumRateList = await this.$axios.$get(`/api/v1/cms/config/area/premium/${params.id}`, {
        params,
      })
      commit('commitPremiumRateList', premiumRateList)
      return premiumRateList
    } catch (e) {
      // throw new Error('Internal Server Error')
    }
  },
  async dispatchSlideList({ commit }, params) {
    try {
      commit('commitSlideList', {})
      const { mainImages } = await this.$axios.$get(`/api/v1/displays/main/visuals`)
      const slideList = mainImages.map((slide) => {
        const linkUrl = slide.linkUrl
        let link = linkUrl && linkUrl.substring(linkUrl.indexOf('.kr/promotions') + 15)
        return {
          ...slide,
          link: linkUrl ? `/promotion/${link}` : '',
        }
      })
      commit('commitSlideList', slideList)
      return slideList
    } catch (e) {
      // throw new Error('Internal Server Error')
    }
  },
  dispatchIsLoading({ commit }, params) {
    console.log(params)
    // commit('commitIsLoading', params)
  },
  dispatchPageName({ commit }, params) {
    commit('commitPageName', params.pageName)
  },
  dispatchRedirectUrl({ commit }, params) {
    commit('commitRedirectUrl', params.redirectUrl)
  },
  dispatchPopup({ commit }, params) {
    commit('commitPopup', {
      ...params,
      show: true,
    })
  },
  dispatchClosePopup({ commit }, params) {
    commit('commitPopup', {
      type: 'alert',
      show: false,
      disableClickOutside: false,
      title: '',
      message: '',
      callback: null,
    })
  },
}

export default {
  state,
  getters,
  mutations,
  actions,
}
