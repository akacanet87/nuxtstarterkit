export default function({ $axios, store, redirect, app, error }) {
  $axios.defaults.baseURL = process.env.API_URL
  $axios.defaults.timeout = 10000
  // $axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
  $axios.defaults.headers.post['Content-Type'] = 'application/json'

  $axios.onError((e) => {
    console.log('Axios 에러났음! e.response.status', e.response.status)
    console.log('Axios 에러났음! e.response.data', e.response.data)
    // redirect(`/error?statusCode=${e.response.status}&message=${e.response.data.error_description}`)
    // return error({ statusCode: e.response.status, message: e.response.data.error_description })
  })

  $axios.interceptors.request.use((config) => {
    // Do something before request is sent
    if ((!config.headers || !config.headers.Authorization || !config.headers.userId) && app.$cookies.get('veltra-token') && app.$cookies.get('veltra-user')) {
      config.headers.Authorization = `Bearer ${app.$cookies.get('veltra-token')}`
      config.headers.userId = app.$cookies.get('veltra-user')
    }
    return config
  }, function(e) {
    // Do something with request error
    return Promise.reject(e)
  })

  // Add a response interceptor
  $axios.interceptors.response.use(function(response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response
  }, function(e) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(e)
  })
}
