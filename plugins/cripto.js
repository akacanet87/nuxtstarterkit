import Crypto from 'crypto-browserify'

const crypto = {
  decodeValue(value) {
    const decodeData = Crypto.createDecipher('aes-128-ecb', 'veltra')
    decodeData.update(value, 'base64', 'utf8')
    return decodeData.final('utf8')
  },
  encodeValue(value) {
    const encodeData = Crypto.createCipher('aes-128-ecb', 'veltra')
    encodeData.update(value, 'utf8', 'base64')
    return encodeData.final('base64')
  },
}

export default crypto
