const withoutAuth = process.env.WITHOUT_AUTH_PAGE
const needAuth = process.env.NEED_AUTH_PAGE

export default function({ store, redirect, error, route }) {
  // if (app.$cookies.get('veltra-token') && app.$cookies.get('veltra-user')) {
  //   store.
  // }

  // 사용자가 인증을 하지 않은 경우.
  console.log('store.state.user', store.state.user)
  // console.log('app.$cookies.get(\'veltra-user\')', app.$cookies.get('veltra-user'))
  console.log('route.path', route.path)
  if (!store.state.user && (needAuth.indexOf(route.path) >= 0)) {
    return redirect(`/auth/login?redirectUrl=${route.path}`)
  } else if (store.state.user && withoutAuth.indexOf(route.path) >= 0) {
    console.log('user 가 있는데 권한이 없어야만 올수 있는 곳에 왔다!!')
    return redirect('/')
  }
  if (route.path) {
    if (route.path.indexOf('/region') === 0 && store.state.region.validRegionList && store.state.region.validRegionList.indexOf(route.path) < 0) {
      return error({ statusCode: 404, message: '유효하지 않은 지역 경로입니다.' })
    } else if (route.path === '/mypage') {
      return redirect('/mypage/profile')
    } else if (route.path === '/login') {
      return redirect(`/auth/login?redirectUrl=${route.path}`)
    } else if (route.path === '/signup') {
      return redirect(`/auth/signup?redirectUrl=${route.path}`)
    } else if (route.path === '/help') {
      return redirect('/help/notice')
    }
  }
}
