const path = require('path')

module.exports = {
  mode: 'universal',
  dev: (process.env.NODE_ENV !== 'production'),
  /*
  ** Headers of the page
  */
  env: {
    // API_URL: (process.env.NODE_ENV !== 'production') ? 'https://dev-api.veltra.kr' : 'https://dev-api.veltra.kr',
    BASE_URL: (process.env.NODE_ENV !== 'production') ? 'https://veltra.kr' : 'https://veltra.kr',
    API_URL: (process.env.NODE_ENV !== 'production') ? 'https://dev-api.veltra.kr' : 'https://api.veltra.kr',
    NAVER_CLIENT_KEY: (process.env.NODE_ENV !== 'production') ? 'YLNB1neqjEWCn3GBYFch' : 'eXqw5EtwltiCfDse4BOT',
    CALLBACK_URL: (process.env.NODE_ENV !== 'production') ? 'http://127.0.0.1:8000/auth/callback' : 'https://dev.veltra.kr/auth/callback',
    TITLE_TEMPLATE: '- 액티비티는 벨트라와 함께',
    KEYWORD: '벨트라, 벨트라코리아, 여행, 투어, 관광, 액티비티, 자유여행, 패키지여행, 여행상품, 티켓, 패스, 즐거움, 행복, 유럽, 아시아, 오세아니아, 아메리카, 아프리카, Veltra, Travel, Tour, Trip, Activity, Activities, Ticket, Pass, Enjoy, Joy',
    DESCRIPTION: '벨트라에서 제공하는 다양한 여행,투어,액티비티 상품으로 한층 더 즐거운 여행을 즐겨보세요',
    IMG_WIDTH: '540',
    IMG_HEIGHT: '520',
    WITHOUT_AUTH_PAGE: ['/auth/login', '/auth/signup', '/auth/signup/social', '/auth/find-password', '/auth/reset-password', '/auth/reset-password/callback', '/auth/callback'],
    NEED_AUTH_PAGE: ['/mypage', '/mypage/booking', '/mypage/wish', '/mypage/coupon', '/mypage/profile', '/booking/result', '/booking/callback', '/reservation/confirm', '/reservation/form'],
  },
  head: {
    title: '벨트라코리아',
    titleTemplate: '%s - 액티비티는 벨트라와 함께',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=no' },
      { hid: 'description', name: 'description', content: process.env.DESCRIPTION },
      { hid: 'keywords', name: 'keywords', content: process.env.KEYWORD },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Noto+Sans+KR:300,400,500,700,900&amp;subset=korean' },
    ],
    script: [
      { type: 'text/javascript', src: 'https://static.nid.naver.com/js/naveridlogin_js_sdk_2.0.0.js', charset: 'UTF-8' },
    ],
  },
  /*
  ** Customize the progress-bar color
  */
  loading: '~/components/common/Loading.vue',
  /*
  ** Global CSS
  */
  css: [
    { src: '@/assets/styles/common.scss', lang: 'scss' },
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/axios',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
    ['cookie-universal-nuxt', { alias: 'cookies' }],
  ],
  styleResources: {
    scss: [
      '@/assets/styles/variables.scss',
    ],
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    // allows our SSR instance to use headers in axios request
    proxyHeaders: false,
    credentials: false,
  },
  resolve: {
    baseUrl: './',
    fallback: [path.join(__dirname, '../node_modules')],
    alias: {
      vue$: 'vue/dist/vue.common.js',
      '@': path.resolve(__dirname),
    },
    extensions: ['', '.js', '.vue', '.json', '.scss'],
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    terser: {
      terserOptions: {
        compress: {
          drop_console: true,
        },
      },
    },
    vendor: [],
    extend(config, ctx) {
    },
  },
  router: {
    mode: `history`,
    middleware: 'checkPage',
    extendRoutes(routes, resolve) {
      routes.push({
        path: '/region/:continent',
        component: resolve(__dirname, 'pages/region/_place.vue'),
        name: 'RegionContinent',
      })
      routes.push({
        path: '/region/:continent/:country',
        component: resolve(__dirname, 'pages/region/_place.vue'),
        name: 'RegionCountry',
      })
      routes.push({
        path: '/region/:continent/:country/:city',
        component: resolve(__dirname, 'pages/region/_place.vue'),
        name: 'RegionCity',
      })
    },
  },
}
